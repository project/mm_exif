
The mm_exif module provides the ability to search through image files like 
.jpg, .jpeg, .wav, .png files finds the exif data and removes it.  Removing
exif data protects user's privacy.  Allowing a user to upload sensitive material
without allowing others to use the uploaded file as a way to track down 
that user.

-----------------------------------------------------------------------
 INSTALLATION
-----------------------------------------------------------------------
This module relies on jhead to find and remove the exif data.
Download and install jhead on your server first:  http://www.sentex.net/~mwandel/jhead/

Next, install the module under sites/all/modules or 
sites/yoursite/modules.  How to install:  http://drupal.org/node/120641

Go to admin/build/modules

Enable the mm_exif module


-----------------------------------------------------------------------
 CONFIGURATION
-----------------------------------------------------------------------

Go to admin/media_mover/settings
These are the global settings for the Media Mover modules

Make sure you configure the path to jhead. This is relative to the root 
of your server. If don't know where jhead is installed and you have
access to the command line of your server, you can run:

#which jhead


-----------------------------------------------------------------------
 USAGE
-----------------------------------------------------------------------

Goto admin/media_mover to build a configuration

Select which files to harvest in the 'harvest configuration' section.  
Do not set it to 'Bypass this operation',
otherwise exif module will not process any files.

In 'process configuration' section, select:

EXIF module:  Find and remove EXIF data.
 
Click Save Configuration, and it is ready to go.
  
Media Mover configurations will be run every time cron is run.
You can run a single configuration by hand by clicking 'run' at 
admin/media_mover for the specified configuration.

